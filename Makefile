.PHONY: create-iac-code create-env create-golang-code create-image

create-env:
	npm install -g cdk8s-cli \
    && npm install -g snyk \
    && brew install go

create-iac-code:
	cd iac \
	&& cdk8s init typescript-app \
	# https://cdk8s.io/docs/latest/getting-started/ copy && past code
	&& npm run compile \
	&& cdk8s synth \
	&& cd ..

create-golang-code:
	go mod init main && go mod tidy && go build -o binary

create-image:
	docker build -f ./Dockerfile -t snyk-node --no-cache .

check-image:
	snyk container test snyk-node \
	&& snyk container monitor snyk-node

check-iac:
	cd iac && snyk iac test ./dist/cdk8s.k8s.yaml && cd ..

check-code:
	cd golang && snyk test && snyk monitor
